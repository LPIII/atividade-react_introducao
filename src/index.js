import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// TODO: refatorar
// Square
// ███████╗ ██████╗ ██╗   ██╗ █████╗ ██████╗ ███████╗
// ██╔════╝██╔═══██╗██║   ██║██╔══██╗██╔══██╗██╔════╝
// ███████╗██║   ██║██║   ██║███████║██████╔╝█████╗  
// ╚════██║██║▄▄ ██║██║   ██║██╔══██║██╔══██╗██╔══╝  
// ███████║╚██████╔╝╚██████╔╝██║  ██║██║  ██║███████╗
// ╚══════╝ ╚══▀▀═╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝
//                                                   
class Square extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: null,
		};
	}


	render() {
		return (
			// botao com funcao de clique que atualiza o estado.
			<button className="square" onClick={() => this.props.onClick()}>
				{this.props.value}
			</button>
		);
	}
}

// TODO: refatorar
// Board
// ██████╗  ██████╗  █████╗ ██████╗ ██████╗ 
// ██╔══██╗██╔═══██╗██╔══██╗██╔══██╗██╔══██╗
// ██████╔╝██║   ██║███████║██████╔╝██║  ██║
// ██╔══██╗██║   ██║██╔══██║██╔══██╗██║  ██║
// ██████╔╝╚██████╔╝██║  ██║██║  ██║██████╔╝
// ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ 
//                                          
class Board extends React.Component {
	renderSquare(i) {
		// Queremos alterar um valor especifoco nesse array, mas o quadrado nõ pode fazer isso pois o estado de board eh privado
		// entao pasamos uma funcao
		return ( 
			<Square
			value={this.props.squares[i]}
			onClick={() => this.props.onClick(i)}
			/>
		);
	}

	render() {
			return (
			<div>
				<div className="board-row">
					{this.renderSquare(0)}
					{this.renderSquare(1)}
					{this.renderSquare(2)}
				</div>
				<div className="board-row">
					{this.renderSquare(3)}
					{this.renderSquare(4)}
					{this.renderSquare(5)}
				</div>
				<div className="board-row">
					{this.renderSquare(6)}
					{this.renderSquare(7)}
					{this.renderSquare(8)}
				</div>
			</div>
		);
	}
}

// game
//  ██████╗  █████╗ ███╗   ███╗███████╗
// ██╔════╝ ██╔══██╗████╗ ████║██╔════╝
// ██║  ███╗███████║██╔████╔██║█████╗  
// ██║   ██║██╔══██║██║╚██╔╝██║██╔══╝  
// ╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗
//  ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝
//                                     
class Game extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			// guardar o historico de jogadas
			history: [{
				squares: Array(9).fill(null)
			}],
			xIsNext: true
		};
	}

	handleClick(i) {

		// pega historico do estado
		const history = this.state.history;
		const current = history[history.length - 1];
		const squares = current.squares.slice();

		// verifica se tem vencedor
		if (calculateWinner(squares) || squares[i]) {
			return;
			// pára
		}
		squares[i] = this.state.xIsNext ? 'X' : 'O';
		// Atualiza o estado
		this.setState({
			history: history.concat([{
				squares: squares
			}]),
			xIsNext: !this.state.xIsNext, //troca a vez
		});
	}


	render() {
		// Trata o historico
		const history = this.state.history;
		const current = history[history.length-1];
		const winner = calculateWinner(current.squares);

		// navegando entre o historico
		const moves = history.map((step, move) => {
			const desc = move ?
				'Go to move #' + move :
				'Go to game start';

			return (
				<li>
					<button onClick={() => this.jumpTo(move)}>{desc}</button>
				</li>
			);
		})


		let status;

		// Indica o vencedor ou de quem eh a vez
		if (winner) {
			status = 'Winner: ' + winner;
		} else {
			status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
		}

		return (
			<div className="game">
				<div className="game-board">
					<Board
						squares={current.squares}
						onClick={(i) => this.handleClick(i)}
					/>
				</div>
				<div className="game-info">
					<div>{status}</div>
					<ol>{moves}</ol>
				</div>
			</div>
		);
	}
}

// ========================================

ReactDOM.render(
	<Game />,
	document.getElementById('root')
);


// Util
// ██╗   ██╗████████╗██╗██╗     
// ██║   ██║╚══██╔══╝██║██║     
// ██║   ██║   ██║   ██║██║     
// ██║   ██║   ██║   ██║██║     
// ╚██████╔╝   ██║   ██║███████╗
//  ╚═════╝    ╚═╝   ╚═╝╚══════╝
//                              
function calculateWinner(squares) {
	const lines = [
		[0, 1, 2],
		[3, 4, 5],
		[6, 7, 8],
		[0, 3, 6],
		[1, 4, 7],
		[2, 5, 8],
		[0, 4, 8],
		[2, 4, 6],
	];
	for (let i = 0; i < lines.length; i++) {
		const [a, b, c] = lines[i];
		if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
			return squares[a];
		}
	}
	return null;
}
