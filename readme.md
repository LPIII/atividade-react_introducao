## Como intalar

```sh
sudo apt-get install -y curl
```

```sh
sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash
```

```sh
sudo apt-get install -y nodejs
```

```sh
sudo npm install -g create-react-app
```


Criando repositório

```sh
sudo create-react-app .
```
    
```sh
npm start
```

Para ter permissão para atualizar os arquivos
```sh
sudo chmod 777 *
```

## atividades:
Começando:
https://reactjs.org/tutorial/tutorial.html

Tutorial básico, desse link:
https://reactjs.org/docs/introducing-jsx.html

Até aqui:
https://reactjs.org/docs/thinking-in-react.html
